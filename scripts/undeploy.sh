#!/bin/bash
# undeploy.sh - uninstalls the rpiled service

function log(){ echo -e "\n\033[1;34m$@\033[0m\n"; }

INSTALL_DIR=/root/rpi-led

log 'Cleaning install directory:'
rm -rfv ${INSTALL_DIR}

log 'Stopping the service'
sudo systemctl stop rpiled || true
sudo rm -f /etc/systemd/system/rpiled.service
sudo systemctl daemon-reload

log 'Service uninstallation complete.'
