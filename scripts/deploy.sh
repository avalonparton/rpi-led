#!/bin/bash
# deploy.sh - installs a service to run the server

function log(){ echo -e "\n\033[1;34m$@\033[0m\n"; }

INSTALL_DIR=/root/rpi-led
HERE="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
sudo mkdir -p ${INSTALL_DIR}

# Clean install dir
log "Cleaning install directory:"
sudo rm -rfv ${INSTALL_DIR}/*

# Move to install dir
log "Moving to install directory:"
sudo mv -v ${HERE}/../static ${INSTALL_DIR}
sudo mv -v ${HERE}/../templates ${INSTALL_DIR}
sudo mv -v ${HERE}/../*.py ${INSTALL_DIR}
sudo mv -v ${HERE}/../requirements.txt ${INSTALL_DIR}

### /etc/systemd/system/rpiled.service
log "Creating /etc/systemd/system/rpiled.service:"
cat << EOF | sudo tee /etc/systemd/system/rpiled.service
[Unit]
Description=RPI-LED
After=multi-user.target
[Service]
User=root
Group=root
Type=idle
WorkingDirectory=${INSTALL_DIR}
ExecStart=/usr/bin/python3 -u ${INSTALL_DIR}/server.py
Restart=always
RestartSec=1
[Install]
WantedBy=multi-user.target
EOF

#### Reload systemctl to trigger new service
sudo systemctl daemon-reload
sudo systemctl stop rpiled || true
sudo systemctl start rpiled.service
sudo systemctl enable rpiled.service

log "Service status:"
systemctl status rpiled

# got locked out of my own machine, let me back in
log "Added root ssh keys"
sudo mkdir -pv /root/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCy/2Z60E1UF0CQcS0oj97Rbl7giMxRjVthSXMu+4ERIzSdg6uiLzPPjb7S6Pb8WfDcZvy8HnY1LY5mm3isCary65qXeVNHp4d2sFO7MsX5I4lkSTjPA6q8LlrjclPF5ehfd/jjUPdkszirDIQSNAJIR6u91vxY9QYUGpIcSv0GNC9rJDH848jHmGNuOgiPl4YhrIUnT/QX8totHt6yZYOt55mUmqI7GES11D/NF/xHZrNeKtFPP2ADhlQrcuFpC1RrYXqWMDjGj37odL62z50lULknJ0tu8ELc3KmTHon0arT0hJ83FtNJfQoZbCR8DfxupWxVacB6L6PIUXJip avalon@DESKTOP-7BHRJQIx' | sudo tee /root/.ssh/authorized_keys

echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCf5W7a+fnSDzyR/7teeeaayrje3fH7fZnXhVFC2Hj65oOq5K/eV9Cjolye1456pp6s2oVUpFw06zciKjSz9LXVK73z2t4mp2pJRgRo5FnOTevgf/REgxh/Ea4+dM+4phEFoy+Av6RDjaIep+PhxO5rQP+ll6fGgwSl1gS7inCBAAoOH9xPzPHMxQ69/Ge02ZDstjM8/CE7qQ4sXY/EpjQ6v/iIvvKFvTjG6ZJGY8T8OmrNvDWt5dbNtTTRp72ONL05qG+BUEmtqM191JOgUSjGttZ/VApX0hvburxfa4fLZVosAAflq9Uk7uBURdcgIeVMdWBZ8GCHgqHhCrYpGn6X avalon@peachx' | sudo tee -a /root/.ssh/authorized_keys



log "/root/.ssh/authorized_keys:"
cat /root/.ssh/authorized_keys

log "Service installation complete."