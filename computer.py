""" Desktop PC controls """

import RPi.GPIO as GPIO


RELAY_PIN = 29

def on():
    """ Toggles the relay if the computer is off """
    GPIO.output(17, GPIO.LOW)
    return 0
