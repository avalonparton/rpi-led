from rpi_ws281x import *
from webcolors import name_to_rgb
import asyncio
import animations
import atexit
import signal


class LEDStrip():
    """ LED Strip that handles animations asynchonously

    Methods:
        solid(color) - sets all leds to a single color
        off()        - sets all leds to black

    Async methods (animations):
        fade(colors, delay_ms) - fade between list of colors
        wipe(colors, delay_ms) - wipe between list of colors
        rainbow(delay_ms)      - cycle through all colors
        night_light(brightness, spread, steps, timeout)
        stop()                 - stop animation (does not turn off)
    """
    def __init__(self, LED_COUNT, LED_PIN=18):
        """ Initialize the physical LED strip """
        print(f"Created LED strip with {LED_COUNT} pixels on pin {LED_PIN}")
        self._strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN)
        self._strip.begin()
        self.length = LED_COUNT
        self._task = None

        # Hit the lights if the script gets killed
        signal.signal(signal.SIGINT, self.off)
        signal.signal(signal.SIGTERM, self.off)
        atexit.register(self.off)

    def solid(self, color):
        """ Instantly sets every LED to a single color """
        self.stop()
        animations.solid(self._strip, color)

    def off(self):
        """ Instantly sets every LED to black """
        self.stop()
        animations.solid(self._strip, 0)

    async def fade(self, colors, delay_ms=50, steps=150):
        """ Fades between a list of colors """
        self.stop()
        animation = animations.fade(self._strip, colors, delay_ms, steps)
        self._task = asyncio.create_task(animation)

    async def wipe(self, colors, delay_ms=0):
        """ Wipes between a list of colors """
        self.stop()
        animation = animations.wipe(self._strip, colors, delay_ms)
        self._task = asyncio.create_task(animation)

    async def night_light(self, brightness=60.0, spread=10, steps=50, timeout=180):
        """ Turns on the night light """
        self.stop()
        animation = animations.night_light(self._strip, brightness, spread, steps, timeout)
        self._task = asyncio.create_task(animation)

    async def rainbow(self):
        """ Turns on a moving rainbow """
        self.stop()
        animation = animations.rainbow(self._strip)
        self._task = asyncio.create_task(animation)

    async def rainbow_fade(self, delay_ms=100):
        """ Fades between colors of the rainbow """
        self.stop()
        animation = animations.rainbow_fade(self._strip, delay_ms)
        self._task = asyncio.create_task(animation)

    def stop(self):
        """ Stops the current animation. Does not turn off leds """
        if self._task:
            self._task.cancel()
            self._task = None


def parse_color_str(color):
    """ Converts a string into a rpi_ws281x.Color()

    Args:
        color: (str) A hex value or color name ('0x00FFAA' or 'green')

    Returns:
        A rpi_ws281x.Color(), or None if the the string is invalid
    """
    try:
        # Hex value: "0x0022FF"
        if len(color) == 8 and '0x' in color:
            r = int(color[2:4], 16)
            g = int(color[4:6], 16)
            b = int(color[6:8], 16)
        # Color name: "red" "turquoise" "lavender"
        else:
            rgb = name_to_rgb(color)
            r = rgb[0]
            g = rgb[1]
            b = rgb[2]
        return Color(r, g, b)
    except ValueError:
        return None


async def main():
    """ Test animations """
    strip = LEDStrip(450)

    color_strs = ['red', 'green', 'blue', '0x00FFAA']

    # Parse all color names
    colors = map(parse_color_str, color_strs)

    while True:

        print("solids")
        for color in colors:
            strip.solid(color)
            await asyncio.sleep(1)

        print("starting the fade")
        await strip.fade(colors, 15, 150)
        print("started the fade")

        await asyncio.sleep(5)
        print("stopping the animations")
        await strip.stop()

        await asyncio.sleep(10)
        print("turning off the strip")
        strip.off()
        await asyncio.sleep(3)


if __name__ == "__main__":
    asyncio.run(main())
