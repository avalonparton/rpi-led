from quart import Quart, render_template, render_template_string, request
import leds
import os

import computer

import roku
TV = roku.Roku("192.168.1.73")

# LED Strip configuration
strip = leds.LEDStrip(LED_COUNT=450, LED_PIN=18)

# Quart app configuration
app = Quart(__name__)


@app.route("/")
async def index():
    return await render_template('index.html', message='Welcome')


@app.route("/led/fade", methods=['GET', 'POST'])
async def led_fade():
    try:
        delay_ms = int(request.args.get('delay_ms', 1))
        steps = int(request.args.get('steps', 100))
    except ValueError:
        return f'Invalid parameters: {request.args}'

    colors = []
    for token in request.args.get('colors').split(' '):
        print("token:", token)
        colors.append(leds.parse_color_str(token))

    if colors:
        print(f"calling strip.fade({colors}, {delay_ms}, {steps})")
        await strip.fade(colors, delay_ms, steps)
        message = f'Fade colors set to {colors} with a delay of {delay_ms} ms'
    else:
        message = 'Failed to set fade colors'

    return message


@app.route("/led/wipe", methods=['GET', 'POST'])
async def led_wipe():
    delay_ms = int(request.args.get('delay_ms', 0))

    colors = []
    for token in request.args.get('colors').split(' '):
        print("token:", token)
        colors.append(leds.parse_color_str(token))

    if colors:
        await strip.wipe(colors, delay_ms)
        message = f'Wipe colors set to {colors} with a delay of {delay_ms} ms'
    else:
        message = 'Failed to set wipe colors'

    return message


@app.route("/led/night", methods=['GET', 'POST'])
async def night_light():
    brightness = float(request.args.get('brightness', 60.0))
    spread = int(request.args.get('spread', 10))
    steps = int(request.args.get('steps', 50))
    timeout = int(request.args.get('timeout', 180))
    await strip.night_light(brightness, spread, steps, timeout)
    return f'Night light turned on with brightness: {brightness} spread: {spread} steps: {steps} timeout: {timeout}'


@app.route("/led/rainbow", methods=['GET', 'POST'])
async def rainbow():
    await strip.rainbow()
    print('Rainbow cycle enabled')
    return 'Rainbow cycle enabled'


@app.route("/led/rainbow_fade", methods=['GET', 'POST'])
async def rainbow_fade():
    delay_ms = int(request.args.get('delay_ms', 100))
    await strip.rainbow_fade(delay_ms)
    print(f'Rainbow Fade enabled with a delay of {delay_ms} ms')
    return f'Rainbow Fade enabled with a delay of {delay_ms} ms'


@app.route("/led/solid", methods=['GET', 'POST'])
async def led_solid():
    color = leds.parse_color_str(request.args.get('color'))
    if color is not None:
        strip.solid(color)
        message = 'Solid color set'
    else:
        message = 'Failed to set solid color'

    return message


@app.route("/led/off", methods=['GET', 'POST'])
async def led_off():
    strip.off()
    return "Turned off the LED strip"


@app.route("/pc", methods=['GET', 'POST'])
async def relay():
    """ Turns on the PC. """
    if os.system(f"ping -c 1 hotbox") == 0:
        return "PC already on"
    else:
        computer.on()
        return "Turning on the PC"

@app.route("/tv", methods=['GET', 'POST'])
async def tv_on():
    """ Turns on the TV. """
    cmd = request.args.get('command')
    if cmd.lower() == 'on':
        TV['Computer'].launch()
        return "Turning on the TV"
    elif cmd.lower() == 'off':
        TV._post('/keypress/PowerOff')
        return "Turning off the TV"
    

@app.route("/tv/off", methods=['GET', 'POST'])
async def tv_off():
    """ Turns off the TV. """
    TV._post('/keypress/PowerOff')
    return "Turning off the TV"


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5001, debug=False)
