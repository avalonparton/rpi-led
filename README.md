# LED Strip Web Control

I use this app to control an LED strip mounted in my bedroom! It also turns my Roku TV on and off.

![screenshot](https://i.imgur.com/McQCtH3.png)

## Running Locally

```bash
git clone git@gitlab.com:avalonparton/rpi-led.git
apt-get install libxslt-dev
pip3 install -r requirements.txt
python3 server.py
```

Your app should now be running on [localhost:5001](http://localhost:5001/).

## Hardware

The LED strip is connected to GPIO pin 18 on a Raspberry pi and is powered by a large 12V power supply.

![hardware](https://i.imgur.com/GYhhGMv.jpg)

## Endpoints

/led/fade

/led/rainbow

/led/solid

/led/wipe
