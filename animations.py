import asyncio
from itertools import cycle
from webcolors import name_to_rgb
from rpi_ws281x import Color


def color_to_rgbw(color):
    """ Converts a 24-bit color to an RGBW color tuple """
    w = (color & 0xFF000000) >> 24
    r = (color & 0x00FF0000) >> 16
    g = (color & 0x0000FF00) >> 8
    b = (color & 0x000000FF)
    return (r, g, b, w)


def random_color():
    return wheel(randint(0, 255))


def solid(strip, color):
    """ Instantly sets every LED to a single color """
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
    strip.show()


def wheel(pos):
    """ Generate rainbow colors across 0-255 positions """
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)


async def fade(strip, colors, delay_ms=15, steps=150):
    """ Fades between multiple colors

    Args:
        strip:    Adafruit LED strip
        colors:   List of hex colors [0x00FFBB, 0xFF00AA]
        delay_ms: Milliseconds between updates
        steps:    Updates between colors

    Loops indefinitely.
    """
    print(f'fade({colors}, {delay_ms}, {steps})')

    # Initialize color to black
    red = 0.0
    green = 0.0
    blue = 0.0
    white = 0.0

    # Cycle through each color in the list
    for color in cycle(colors):
        (r, g, b, w) = color_to_rgbw(color)
        # print(f"next color: {(r, g, b, w)}")

        # make steps to get from one color to another
        stepR = (r - red) / steps
        stepG = (g - green) / steps
        stepB = (b - blue) / steps
        stepW = (w - white) / steps

        # print(f"steps: {(stepR, stepG, stepB, stepW)}")

        # change current_color, moving toward next_color
        for i in range(steps):
            # update the strip with new current_color
            col = Color(int(red), int(green), int(blue), int(white))
            solid(strip, col)

            # Go on to the next step
            red += stepR
            green += stepG
            blue += stepB
            white += stepW
            await asyncio.sleep(delay_ms / 1000)


async def night_light(strip, brightness=60.0, spread=10, steps=50, timeout=180):
    """ Night light that fades out over time """
    print(f'night_light({brightness}, {spread}, {steps}, {timeout})')
    solid(strip, 0)  # Turn off the strip
    delay = timeout / steps  # 6 seconds
    while brightness > 0:
        color = Color(int(brightness), 0, 0)
        # Set one in every {spread} LEDs
        for i in range(strip.numPixels()):
            if (i % spread == 0):
                strip.setPixelColor(i, color)
        strip.show()
        await asyncio.sleep(delay)
        brightness = brightness - (brightness/steps)
    # Turn it off
    solid(strip, 0x000000)


async def rainbow(strip, delay_ms=15):
    """ Draw rainbow that uniformly distributes itself across all pixels """
    while True:
        for j in range(256):
            for i in range(strip.numPixels()):
                color = wheel((int(i * 256 / strip.numPixels()) + j) & 255)
                strip.setPixelColor(i, color)
            strip.show()
            await asyncio.sleep(delay_ms / 1000)

async def rainbow_fade(strip, delay_ms=100):
    """ Fade between colors of the rainbow """
    while True:
        for i in range(256):
            solid(strip, wheel(i))
            await asyncio.sleep(delay_ms / 1000)


async def sprinkle(strip, delay_ms=20):
    """ Sets one random pixel at a time to random colors """
    n = strip.numPixels()
    while True:
        pixel = randint(0, n)
        strip.setPixelColor(pixel, random_color())
        strip.show()
        await asyncio.sleep(delay_ms / 1000)


async def wipe(strip, colors, delay_ms=1):
    """ Wipe colors across display one pixel at a time """
    print(f'wipe({colors}, {delay_ms})')
    n = strip.numPixels()
    for color in cycle(colors):
        for i in range(n):
            strip.setPixelColor(i, color)
            strip.show()
            await asyncio.sleep(delay_ms / 1000)
